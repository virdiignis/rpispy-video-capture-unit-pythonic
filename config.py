# Directory for videos
#VIDEO_PATH = '/media/usb/'
VIDEO_PATH = '/home/pi/rpispy_vcu/videos/'
LOG_PATH   = '/home/pi/rpispy_vcu/'
# Recording settings
VIDEO_COUNT      = 18
VIDEO_INTERVAL   = 300

# Camera properties
VIDEO_LED        = False
VIDEO_VFLIP      = False
VIDEO_FRAMERATE  = 25
VIDEO_ROTATE     = 0
#VIDEO_RESOLUTION = ( 640, 360)
VIDEO_RESOLUTION = (1280, 720)
#VIDEO_RESOLUTION = (1920, 1080)

# GPIO pins
BUZZERGPIO = 8
BUTTONGPIO = 7
POWERLED = 4
ACTIVLED = 11

# Shutdown Pi when script exits on button press
AUTO_SHUTDOWN = True

# Run if network available
AUTORUN_IF_LAN = False