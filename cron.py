#!/usr/bin/python
import os
import time
import shutil
import subprocess


# Function definitions
def detect_network(interface='eth0'):
    """
    Read ifconfig.txt and determine
    if network is connected
    :param interface:
    :return:
    """
    try:
        # Get network details using ifconfig
        # Check network details to see if interface exists
        if 'inet addr:' in subprocess.getoutput(["/sbin/ifconfig", interface]).split('\n')[1]:
            return True
        else:
            return False
    except:
        return False


if os.path.exists('/boot/vcu_config.py'):  # Config file exists in boot partition
    shutil.copyfile('/boot/vcu_config.py', '/home/pi/rpispy_vcu/config.py')

if os.path.exists('/home/pi/rpispy_vcu/config.py'):
    # Run main script
    import config

    # Short delay to give network time to connect
    time.sleep(2)

    network = detect_network('eth0')

    # Run? | LAN | Flag
    # -------------------
    #   1  |  0  |  1  |
    #   1  |  0  |  0  |
    #   1  |  1  |  1  |
    #   0  |  1  |  0  |

    if not network or config.AUTORUN_IF_LAN:
        subprocess.Popen("sudo python /home/pi/rpispy_vcu/vcu.py 1", shell=True)
