REM Loop through h264 files in current directory
REM and using MP4Box create MP4 files using
REM specified frame rate.
REM MP4Box can be installed via :
REM http://gpac.wp.mines-telecom.fr/downloads/gpac-nightly-builds/

set frate=25

For %%A in (*.h264) do (
  MP4Box -fps %frate% -add "%%A" "%%~nA".mp4
)
pause

REM www.RaspberryPi-Spy.co.uk
REM September 2014